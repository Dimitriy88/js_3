//перша задачка
const person=  {
    name : 'John',
    age : 30,
    gender : 'male'
};
console.log(person);

const newPerson = Object.assign({},person)
newPerson.age = 35;
console.log(newPerson);

//друга задачка
const car = {
    make: 'Toyota',
    model: 'Corolla',
    year: 2015
};
function printCarInfo(car) {
    console.log(car)
    if (car.year < 2001) {
        console.log('Машина занадто стара')
    }
}
printCarInfo(car);

//третя задачка
const str = 'JavaScript is a high-level programming language';
function countWords(str) {
    return str
        .split(/[\s-]+/).length
}
console.log(countWords(str));

//четверта задачка
const str2 = 'JavaScript is awesome!';
function reverseString(str2) {
    return str2.split("").reverse().join("");
}
console.log(reverseString(str2));

//пята задачка
const str3 = 'JavaScript is awesome!';
function reverseWordsInString() {
    let reverseWordArr = str3.split(" ").map(word => word.split("").reverse().join(""));
    return reverseWordArr.join(" ");
}
console.log(reverseWordsInString())

